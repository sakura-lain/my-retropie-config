#!/bin/bash
#Script permettant de monter les roms située sur une clé USB labelisée RETROPIE dans le dossier /home/$USER/Retropie

if [ -e /media/$USER/RETROPIE ]
then
	#sudo umount LABEL=RETROPIE
	sudo mount LABEL=RETROPIE /home/$USER/RetroPie
else
	sudo mount LABEL=RETROPIE /home/$USER/RetroPie
fi
